import Select from "./components/Select.js";
import {info} from "./form/info.js";
import ModalForm from "./form/FormModal";
import {editCard, deleteCard} from "./CardsAPI.js";
import {render} from "./render";

export class Visit {
    constructor({firstName, lastName, middleName, doctor, purpose, description, urgency, id, title, status}) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.doctor = doctor;
        this.purpose = purpose;
        this.description = description;
        this.urgency = urgency;
        this.id = id;
        this.title = title;
        this.status = status;
        this.elem = {
            firstName: document.createElement("h3"),
            lastName: document.createElement("h3"),
            middleName: document.createElement("h3"),
            doctor: document.createElement("span"),
            purpose: document.createElement("span"),
            description: document.createElement("p"),
            urgency: document.createElement("span"),
            status: document.createElement("span"),
            showMoreBtn: document.createElement("button"),
            editBtn: document.createElement("button"),
            select: new Select(info.cardEdit).create()
        };
    }

    render(parent) {
        this.elem.firstName.textContent = this.firstName;
        this.elem.firstName.textContent = this.lastName;
        this.elem.firstName.textContent = this.middleName;
        this.elem.doctor.textContent = `Доктор: ${this.doctor}`;
        this.elem.purpose.textContent = `Цель визита: ${this.purpose}`;
        this.elem.description.textContent = `Описание визита: ${this.description}`;
        this.elem.urgency.textContent = `Срочность: ${this.urgency}`;
        this.elem.status.textContent = `Статус: ${this.status}`;
        this.elem.showMoreBtn.textContent = "Расширить";
        this.elem.editBtn.textContent = "Скрыть";
        this.elem.editBtn.style.display = 'none';

        this.elem.self.classList.add("visit");
        this.elem.firstName.classList.add("visit__head");
        this.elem.lastName.classList.add("visit__head");
        this.elem.middleName.classList.add("visit__head");
        this.elem.doctor.classList.add("visit__text");
        this.elem.purpose.classList.add("visit__text");
        this.elem.description.classList.add("visit__text");
        this.elem.urgency.classList.add("visit__text");
        this.elem.status.classList.add("visit__text");
        this.elem.showMoreBtn.classList.add("visit__btn");
        this.elem.editBtn.classList.add("visit__btn");

        this.elem.self.draggable = true;
        this.elem.self.dataset.id = this.id;

        this.elem.select.addEventListener("change", async (event) => {
            if (event.target.value === "Edit") {
                const form = new ModalForm();
                form.ifEditModal(this.id);
                form.render();
            } else if (event.target.value === "Remove") {
                const response = await deleteCard(this.id).then(r => r.json());

                if (response.status === "Success") {
                    this.elem.self.remove();
                    const renderedVisits = document.querySelectorAll(".visit");

                    if (!renderedVisits || renderedVisits.length === 0) {
                        const noItem = document.createElement('p');
                        noItem.id = "empty";
                        noItem.textContent = "No item has been added";
                        parent.append(noItem);
                    }
                }
            }
        });

        this.elem.self.append(this.elem.firstName, this.elem.lastName,this.elem.middleName, this.elem.doctor, this.elem.showMoreBtn, this.elem.editBtn, this.elem.select);
    }
}

export class Cardiologist extends Visit {
    constructor({firstName, lastName, middleName, doctor, purpose, description, urgency,id, title, status, pressure, bodyMassIndex, diseases, age}) {
        super({firstName, lastName, middleName, doctor, purpose, description, urgency, id, title, status});
        this.pressure = pressure;
        this.bodyMassIndex = bodyMassIndex;
        this.diseases = diseases;
        this.age = age;
    }

    render(parent) {
        super.render(parent);
        this.elem.pressure = document.createElement("span");
        this.elem.bodyMassIndex = document.createElement("span");
        this.elem.diseases = document.createElement("span");
        this.elem.age = document.createElement("span");

        this.elem.pressure.textContent = `Давление: ${this.pressure}`;
        this.elem.bodyMassIndex.textContent = `Индекс массы тела: ${this.bodyMassIndex}`;
        this.elem.diseases.textContent = `Ранее перенесенные заболевания сердца: ${this.diseases}`;
        this.elem.age.textContent = `Возраст: ${this.age}`;

        this.elem.pressure.classList.add("visit__text");
        this.elem.bodyMassIndex.classList.add("visit__text");
        this.elem.diseases.classList.add("visit__text");
        this.elem.age.classList.add("visit__text");

        this.elem.showMoreBtn.addEventListener("click", () => {
            this.showMore();
        });
        this.elem.editBtn.addEventListener("click", () => {
            this.hide();
        });

        if (parent) {
            parent.append(this.elem.self);
        } else {
            return this.elem.self;
        }
    }

    showMore() {
        const moreInfo = [];

        for (let key in this.elem) {
            if (key === "purpose" || key === "description" || key === "urgency" || key === "status" || key === "pressure" || key === "bodyMassIndex" || key === " diseases" || key === "age") {
                moreInfo.push(this.elem[key]);
            }
        }
        moreInfo.forEach(item => {
            this.elem.self.insertBefore(item, this.elem.showMoreBtn);
        });
        this.elem.showMoreBtn.style.display = 'none';
        this.elem.editBtn.style.display = 'inline-block';
    }
    hide() {
        this.elem.self.removeChild(this.elem.purpose);
        this.elem.self.removeChild(this.elem.description);
        this.elem.self.removeChild(this.elem.urgency);
        this.elem.self.removeChild(this.elem.status);
        this.elem.self.removeChild(this.elem.pressure);
        this.elem.self.removeChild(this.elem.bodyMassIndex);
        this.elem.self.removeChild(this.elem.diseases);
        this.elem.self.removeChild(this.elem.age);
        this.elem.editBtn.style.display = 'none';
        this.elem.showMoreBtn.style.display = 'inline-block';
    }
}

export class Dentist extends Visit {
    constructor({firstName, lastName, middleName, doctor, purpose, description, urgency,id, title, status, lastDateVisit}) {
        super({firstName, lastName, middleName, doctor, purpose, description, urgency, id, title, status});
        this.lastDateVisit = lastDateVisit;
    }
    render(parent) {
        super.render(parent);
        this.elem.lastDateVisit = document.createElement("span");
        this.elem.lastDateVisit.textContent = `Дата последнего визита: ${this.lastDateVisit}`;
        this.elem.lastDateVisit.classList.add("visit__text");


        this.elem.showMoreBtn.addEventListener("click", () => {
            this.showMore();
        });
        this.elem.editBtn.addEventListener("click", () => {
            this.hide();
        });
        if (parent) {
            parent.append(this.elem.self);
        } else {
            return this.elem.self;
        }
    }

    showMore() {
        const moreInfo = [];

        for (let key in this.elem) {
            if (key === "purpose" || key === "description" || key === "urgency" || key === "status" || key === "lastDateVisit") {
                moreInfo.push(this.elem[key]);
            }
        }

        moreInfo.forEach(item => {
            this.elem.self.insertBefore(item, this.elem.showMoreBtn);
        });

        this.elem.showMoreBtn.style.display = 'none';
        this.elem.editBtn.style.display = 'inline-block';
    }
    hide() {
        this.elem.self.removeChild(this.elem.purpose);
        this.elem.self.removeChild(this.elem.description);
        this.elem.self.removeChild(this.elem.urgency);
        this.elem.self.removeChild(this.elem.status);
        this.elem.self.removeChild(this.elem.lastDateVisit);
        this.elem.editBtn.style.display = 'none';
        this.elem.showMoreBtn.style.display = 'inline-block';
    }
}

export class Therapist extends Visit {
    constructor({firstName, lastName, middleName, doctor, purpose, description, urgency, id, title, status, age}) {
        super({firstName, lastName, middleName, doctor, purpose, description, urgency, id, title, status});
        this.age = age;
    }

    render(parent) {
        super.render(parent);
        this.elem.age = document.createElement("span");
        this.elem.age.textContent = `Возраст: ${this.age}`;
        this.elem.age.classList.add("visit__text")

        this.elem.showMoreBtn.addEventListener("click", () => {
            this.showMore();
        });
        this.elem.editBtn.addEventListener("click", () => {
            this.hide();
        });

        if (parent) {
            parent.append(this.elem.self);
        } else {
            return this.elem.self;
        }
    }

    showMore() {
        const moreInfo = [];

        for (let key in this.elem) {
            if (key === "purpose" || key === "description" || key === "urgency" || key === "status" || key === "age") {
                moreInfo.push(this.elem[key]);
            }
        }
        moreInfo.forEach(item => {
            this.elem.self.insertBefore(item, this.elem.showMoreBtn);
        });

        this.elem.showMoreBtn.style.display = 'none';
        this.elem.editBtn.style.display = 'inline-block';
    }
    hide() {
        this.elem.self.removeChild(this.elem.purpose);
        this.elem.self.removeChild(this.elem.description);
        this.elem.self.removeChild(this.elem.urgency);
        this.elem.self.removeChild(this.elem.status);
        this.elem.self.removeChild(this.elem.age);
        this.elem.editBtn.style.display = 'none';
        this.elem.showMoreBtn.style.display = 'inline-block';
    }
}


