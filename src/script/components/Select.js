export default class Select {
    constructor(optionsArr) {
        this.options = optionsArr;
        this.self = document.createElement("select");
    }
    create() {
        this.options.forEach(options => {
            const optionNode = document.createElement("option");
            optionNode.textContent = options;
            this.self.append(optionNode);
        });
        return this.self;
    }
}





