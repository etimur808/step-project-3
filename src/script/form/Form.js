import Input from "../components/Input.js"
import TextArea from "../components/TextArea.js"
import {info} from "./info.js"
import Select from "../components/Select";
import {createCard, editCard} from "../CardsAPI.js";
import {Dentist, Cardiologist, Therapist} from "../Visit.js";


export default class Form{
    constructor() {
        this.self = document.createElement("form");
        this.firstName = new Input(info.firstName, "form__input").create();
        this.lastName = new Input(info.lastName, "form__input").create();
        this.middleName = new Input(info.middleName, "form__input").create();
        this.purpose = new Input(info.purpose, "form__input").create();
        this.description = new TextArea(info.description, "form__input").create();
        this.urgency = new Select(info.urgency).create();
        this.status = new Select(info.status).create();
        this.create = new Input(info.create, "form__submit").create();
    }

    render(modal) {
        this.self.classList.add("form");


        // console.log(this.create);
        this.create.addEventListener("click", async (event) => {
            event.preventDefault();

            const newCardOut = this.collectData(),
                modal = document.querySelector(".header__modalWrapper");
            if (this.isDataFilled(newCardOut)) {
                if (!modal.dataset.id) {
                    const newCardIn = await createCard(newCardOut).then(r => r.json());
                    this.submitForm(newCardIn, modal);
                } else {
                    const editedCard = await editCard(newCardOut, modal.dataset.id).then(r => r.json());
                    this.submitEdit(editedCard, modal);
                }
            }
        });
        this.self.append(this.firstName,this.lastName,this.middleName, this.purpose, this.description, this.urgency, this.status, this.create);
        modal.append(this.self);
    }

    collectData() {
        const data = {};
        for (let key in this) {
            if (key === "self" || key === "create") {
                continue;
            } else if (key === "doctor") {
                data[key] = this[key];
            } else {
                data[key] = this[key].value;
            }
        }
        return data;
    }


    isDataFilled(objectToCheck) {

        const doctor = document.getElementsByClassName('header__doctor')[0];
        console.log(doctor.value);
        delete objectToCheck.create;
        for (let item in objectToCheck) {

            // console.log(objectToCheck[item]);

                if (objectToCheck[item] === undefined || objectToCheck[item] === null || objectToCheck[item] === ""
                    || objectToCheck[item] === "Выберите срочность" || objectToCheck[item] === "Выберите статус") {
                    console.error(`Missing property ${item}`)
                    return false;
                }

        }
        return true;
    }
    submitForm(newCardIn, modal) {
        const noItem = document.getElementById("empty");
        if (noItem) {
            noItem.remove();
        }
        if (newCardIn.doctor === "Кардиолог") {
            const visit = new Cardiologist(newCardIn);
            visit.render(document.getElementById("container"));
            modal.remove();
        } else if (newCardIn.doctor === "Стоматолог") {
            const visit = new Dentist(newCardIn);
            visit.render(document.getElementById("container"));
            modal.remove();
        } else if (newCardIn.doctor === "Терапевт") {
            const visit = new Therapist(newCardIn);
            visit.render(document.getElementById("container"));
            modal.remove();
        }
    }
    submitEdit(newCardIn, modal) {
        const existCard = [...document.querySelectorAll(".visit")].filter(visit => {
                if (visit.dataset.id === modal.dataset.id) {
                    return visit;
                }
            })[0],
            parent = document.getElementById("container");
        if (newCardIn.doctor === "Кардиолог") {
            const visit = new Cardiologist(newCardIn),
                visitNode = visit.render();
            parent.replaceChild(visitNode, existCard);
            modal.remove();
        } else if (newCardIn.doctor === "Стоматолог") {
            const visit = new Dentist(newCardIn),
                visitNode = visit.render();
            parent.replaceChild(visitNode, existCard);
            modal.remove();
        } else if (newCardIn.doctor === "Терапевт") {
            const visit = new Therapist(newCardIn),
                visitNode = visit.render();
            parent.replaceChild(visitNode, existCard);
            modal.remove();
        }
    }
}
