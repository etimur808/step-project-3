import Input from "../components/Input.js"
import TextArea from "../components/TextArea.js"
import {info} from "./info.js"
import Form from "./Form.js"

export default class FormCardiologist extends Form {
    constructor(doctor) {
        super(doctor);
        this.pressure = new Input(info.pressure, "form__input").create();
        this.bodyMassIndex = new Input(info.bodyMassIndex, "form__input").create();
        this.diseases = new TextArea(info.diseases, "form__input").create();
        this.age = new Input(info.age, "form__input").create();
    }

    render(modal) {
        super.render(modal);
        const nodes = [this.pressure, this.bodyMassIndex, this.diseases, this.age];
        nodes.forEach(node => {
            this.self.insertBefore(node, this.create);
        })
    };
}