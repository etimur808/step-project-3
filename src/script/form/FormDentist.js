import Input from "../components/Input.js"
import Form from "./Form.js"
import {info} from "./info.js"

export default class FormDentist extends Form {
    constructor() {
        super()
        this.lastVisitDate = new Input(info.lastDateVisit, null).create();
    }
    render(modal) {
        super.render(modal);
        this.self.insertBefore(this.lastVisitDate, this.create);
    };
}
