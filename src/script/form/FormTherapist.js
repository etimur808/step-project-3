import Input from "../components/Input.js"
import Form from "./Form.js"
import {info} from "./info.js"

export default class FormTherapist extends Form {
    constructor() {
        super()
        this.age = new Input(info.age, null).create();
    }
    render(modal) {
        super.render(modal);
        this.self.insertBefore(this.age, this.create);
    }
}