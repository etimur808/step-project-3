export const info = {
    firstName: {
        type: "text",
        placeholder: "Имя",
        isRequired: true
    },
    lastName: {
        type: "text",
        placeholder: "Фамилия",
        isRequired: true
    },
    middleName: {
        type: "text",
        placeholder: "Отчество",
        isRequired: true
    },
    purpose: {
        type: "text",
        placeholder: "Цель визита",
        isRequired: true
    },
    description: {
        placeholder: "Коментарии к визиту",
        isRequired: true
    },
    pressure: {
        type: "text",
        placeholder: "Обычное давление",
        isRequired: true
    },
    bodyMassIndex: {
        type: "text",
        placeholder: "Индекс массы тела",
        isRequired: true
    },
    diseases: {
        placeholder: "Ранее перенесённые заболевания сердца",
        isRequired: true
    },
    age: {
        type: "text",
        placeholder: "Возраст",
        isRequired: true
    },
    lastDateVisit: {
        type: "text",
        placeholder: "Дата последнего визита",
        isRequired: true
    },
    create: {
        type: "submit",
        placeholder: 'create',
        isRequired: false
    },
    urgency: [
        "Обычная",
        "Приоритетная",
        "Неотложная",
    ],
    cardEdit: [
        "Опции редактирования",
        "Редактировать",
        "Удалить",
    ],
    status: [
        "Выберите статус",
        "Открыт",
        "Закрыт"
    ]

}


