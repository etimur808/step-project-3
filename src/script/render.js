import {getCards} from "./CardsAPI";
import {Cardiologist, Dentist, Therapist} from "./Visit";

export function render(container) {
    getCards().then(c => c.json())
        .then(visits => {
            // console.log(visits);
            if(visits.length === 0){
                const noItem = document.createElement('p');
                noItem.innerText = "No item has been added";
                noItem.id = "empty";
                container.append(noItem);
            }else {
                let objectVisit = visits.map(v => {
                    if (v.doctor === "Стоматолог") {
                        let newVisit = new Dentist(v);
                        newVisit.render(container);
                        return newVisit;
                    } else if (v.doctor === "Кардиолог") {
                        let newVisit = new Cardiologist(v);
                        newVisit.render(container);
                        return newVisit;
                    } else if (v.doctor === "Терапевт") {
                        let newVisit = new Therapist(v);
                        newVisit.render(container);
                        return newVisit;
                    }
                })
                return objectVisit;
            }
        });
}