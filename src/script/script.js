import {render} from "./render";
import {search} from "./search";
import {token} from "./enter";

const container = document.getElementById("container");
const formContainer = document.querySelector(".section");

if (token) {
    render(container);}

search(formContainer, container);



