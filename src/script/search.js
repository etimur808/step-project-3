import "./CardsAPI"
import {createCard, deleteCard, getCards} from "./CardsAPI";
import {Dentist, Cardiologist, Therapist} from "./Visit";

const container = document.getElementById("container");

export function search(formContainer, visitContainer) {

    const searchWrap = document.createElement('form');
    const searchInput = document.createElement('input');
    const statusInput = document.createElement('select');
    const statusInput1 = document.createElement('option');
    const statusInput2 = document.createElement('option');
    const statusInput3 = document.createElement('option');
    const urgencyInput = document.createElement('select');
    const urgencyInput1 = document.createElement('option');
    const urgencyInput2 = document.createElement('option');
    const urgencyInput3 = document.createElement('option');
    const urgencyInput4 = document.createElement('option');
    const buttonInput = document.createElement('input');

    searchInput.placeholder = "Поиск";
    statusInput1.innerText = "Все";
    statusInput2.innerText = "Открыт";
    statusInput3.innerText = "Закрыт";
    statusInput.append(statusInput1, statusInput2, statusInput3);

    urgencyInput1.innerText = "Все";
    urgencyInput2.innerText = "Обычная";
    urgencyInput3.innerText = "Приоритетная";
    urgencyInput4.innerText = "Неотложная";
    urgencyInput.append(urgencyInput1, urgencyInput2, urgencyInput3, urgencyInput4);

    buttonInput.type = "button";
    buttonInput.value = "Поиск";

    searchWrap.className = 'section__form-wrapper';
    searchInput.className = 'section__form-search';
    statusInput.className = 'section__form-select1';
    urgencyInput.className = 'section__form-select2';
    buttonInput.className = "section__form-btn";

    searchWrap.addEventListener('submit', (e) => {
        e.preventDefault();
        getAndRender();
    });


    searchWrap.append(searchInput, statusInput, urgencyInput, buttonInput);
<<<<<<< HEAD
    // formContainer.prepend(searchWrap);
    document.getElementsByClassName("section")[0].before(searchWrap);
=======

    document.getElementById("container").before(searchWrap);
>>>>>>> ed686f618f4e6dc583bbf3d5a61bf4c15f5c488a

    function getAndRender(){
        visitContainer.innerText = "";
        const visits = getVisits();
        visits.then(cards => {
            let cardsSearch = cards.filter(e => {
                let searchContent = e.doctor + " " + e.purpose + " " + e.firstName + " " + e.lastName + "" + e.middleName;
                if (searchContent.toLowerCase().includes(searchInput.value.toLowerCase()) || searchInput.value === "")
                    if (statusInput.value === "Все" && urgencyInput.value === "Все")
                        return true
                    else
                        return (statusInput.value === e.status && urgencyInput.value === e.urgency) || (statusInput.value === "Все" && urgencyInput.value === e.urgency)
                            || (urgencyInput.value === "Все" && statusInput.value === e.status);
            });
            cardsSearch.forEach((c) => c.render(visitContainer));
        })
    }
    buttonInput.addEventListener('click', (e) => {
        getAndRender();
    })
}

export function getVisits() {
    return getCards().then(c => c.json())
        .then(visits => {
            if (visits !== undefined) {
                let objectVisit = visits.map(v => {
                    if (v.doctor === "Стоматолог") {
                        return new Dentist(v);
                    } else if (v.doctor === "Кардиолог") {
                        return new Cardiologist(v);
                    } else if (v.doctor === "Терапевт") {
                        return new Therapist(v);
                    }
                })
                return objectVisit;
            }
        });
}
